<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    public function user()
    {
        return $this->hasMany('App\Property', 'owner_id');
    }

    public function propertyCount($user_id)
    {
        return count(Property::where('owner_id', $user_id)->get());
    }
}
