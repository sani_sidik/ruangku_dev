<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function isOwner()
    {
        if (Auth::User()->role == 'owner') {
            return true;
        }
        return false;
    }
    public function property()
    {
        return $this->hasMany('App\Property', 'owner_id');
    }

    public function propertyCount($user_id)
    {
        return count(Property::where('owner_id', $user_id)->get());
    }
    public function allRoomCount($user_id)
    {
        $total = 0;
        $user = Auth::user();

        $property = Property::where('owner_id', $user_id)->get();
        foreach ($property as $val) {
            $total += count(Room::where('property_id', $val->id)->get());
        }

        return $total;
    }
}
