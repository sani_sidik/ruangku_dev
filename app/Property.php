<?php

namespace App;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'property';
    protected $fillable = ['name', 'address', 'city_id', 'owner_id', 'total_room', 'type', 'description'];

    public function room()
    {
        return $this->hasMany('App\Room', 'property_id');
    }

    public function roomCount($property_id)
    {
        return count(Room::where('property_id', $property_id)->get());
    }
    public function emptyRoom($property_id)
    {
        return count(Room::where('property_id', $property_id)->where('status', 0)->get());
    }

    public function persentation($id)
    {
        $user = Auth::user();
        $property = Property::find($id);

        $roomCount = Room::where('property_id', $property->id)->count();
        $bookRoom = Room::where('property_id', $property->id)->where('status', 1)->count();
        return ($bookRoom/$roomCount)*100;
    }
}
