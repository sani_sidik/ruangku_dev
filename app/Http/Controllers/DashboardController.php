<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;
use Auth;

class DashboardController extends Controller
{
    public function index()
    {
        return "yes";
    }

    //owner Dashborad
    public function owner()
    {
        if (!Auth::User()->isOwner()) {
            return redirect('user-dashboard');
        }

        $user = Auth::User();
        $this->data['user'] = $user;
        $property = Property::where('owner_id', $user->id)->get();
        $this->data['property'] = $property;
        return view('owner.dashboard', $this->data);
    }

    public function ownerProperty()
    {
        $user = Auth::User();
        $this->data['user'] = $user;
        $property = Property::where('owner_id', $user->id)->get();
        $this->data['property'] = $property;
        return view('owner.property', $this->data);
    }
    public function ownerRental()
    {
        $user = Auth::User();
        $this->data['user'] = $user;
        $property = Property::where('owner_id', $user->id)->get();
        $this->data['property'] = $property;
        return view('owner.rental', $this->data);
    }
    public function ownerRentalid($id)
    {
        $user = Auth::User();
        $this->data['user'] = $user;
        $property = Property::where('owner_id', $user->id)->get();
        $this->data['property'] = $property;
        $customProperty = Property::where('owner_id', $user->id)->where('id', $id)->get();
        $this->data['customProperty'] = $customProperty;
        return view('owner.rental-custom', $this->data);
    }
    public function ownerReports()
    {
        $user = Auth::User();
        $this->data['user'] = $user;
        return view('owner.reports', $this->data);
    }
    public function ownerBilling()
    {
        $user = Auth::User();
        $this->data['user'] = $user;
        return view('owner.billing', $this->data);
    }


    //user Dashboard
    public function user()
    {
        if (Auth::User()->isOwner()) {
            return redirect('owner-dashboard');
        }

        return view('user.dashboard');
    }
}
