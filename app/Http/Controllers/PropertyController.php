<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use App\Room;
use App\Facility;
use Auth;
use DB;

class PropertyController extends Controller
{
    public $data;

    public function __construct()
    {
        $this->data['facility'] = Facility::all();
    }

    public function addProperty(Request $request)
    {
        $this->validate($request, [
            'name' => 'required', 
            'address' =>'required',
            'total_room' => 'required',
            'type'=>'required'
        ]);
        
        $user = Auth::user();
        $getProperty = Property::where('owner_id',$user->id)->get();
        if(count($getProperty)>=4)
        {
            return redirect()->back()->with("msgerr", trans('Your Limit Property Is 4 Please Upgrade Your account'))->withInput();
        }
        DB::beginTransaction();
        try {
  
            $newProperty = new Property();

            $newProperty->name = $request->name;
            $newProperty->address = $request->address;
            $newProperty->owner_id = $user->id;
            $newProperty->city_id = $request->city_id;
            $newProperty->total_room = $request->total_room;
            $newProperty->type = $request->type;
            $newProperty->description = $request->description;
            $newProperty->save();

            $room = $request->total_room;
            for ($a = 0; $a < $room; $a++) {
                $newRoom = new Room();
                $newRoom->property_id = $newProperty->id;
                $newRoom->save();
            }
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            return redirect()->back()->with("msgerr", trans('Failed Delete Room'))->withInput();
        }
        DB::commit();
        return redirect()->back()->with('msg', 'add property success');
    }

    public function detailPropertty($id)
    {
        $user = Auth::user();
        $detail = Property::find($id);
        $room = Room::where('property_id', $detail->id)->get();
        $this->data['user'] = $user;
        $this->data['room'] = $room;
        $this->data['detail'] = $detail;
        return view('owner.detail-property', $this->data);
    }
    public function editProperty(Request $request)
    {

        $user = Auth::user();
        DB::beginTransaction();
        try {
            $updateProperty = Property::find($request->id);

            $updateProperty->name = $request->name;
            $updateProperty->address = $request->address;
            $updateProperty->owner_id = $user->id;
            $updateProperty->city_id = $request->city_id;
            $updateProperty->type = $request->type;
            $updateProperty->description = $request->description;
            $updateProperty->save();
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            return redirect()->back()->with("msgerr", trans('Failed Delete Room'))->withInput();
        }
        DB::commit();
        return redirect()->back()->with('msg', 'Edit property success');
    }

    public function deleteProperty(Request $request)
    {

        $user = Auth::user();
        DB::beginTransaction();
        try {
  
            $property = Property::find($request->id);
            $rooms = Room::where('property_id', $property->id)->get();
            foreach($rooms as $val)
            {
                $delete = Room::find($val->id);
                $delete->delete();
            }
            $property->delete();
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            return redirect()->back()->with("msgerr", trans('Failed Delete Room'))->withInput();
        }
        DB::commit();

        return redirect()->back()->with('msg', 'Delete property success');
    }

    public function getProperty(Request $request)
    {

        $property = Property::where("id", $request->id)->get();

        return response()->json($property->toArray())->setStatusCode(200);
    }

}
