<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use App\Room;
use Auth;
use DB;

class RoomController extends Controller
{
    public function addRoom(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'facilities' => 'required'
        ]);

        $facilities = implode("|", $request->facilities);

        DB::beginTransaction();
        try {
            $newRoom = new Room();
            $newRoom->name = $request->name;
            $newRoom->property_id = $request->property_id;
            $newRoom->price = $request->price;
            $newRoom->facilities = $facilities;
            $newRoom->save();

            $updateProperty = Property::find($request->property_id);
            $updateProperty->total_room += 1;
            $updateProperty->save();
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            return redirect()->back()->with("msgerr", trans('Failed Add Room'))->withInput();
        }
        DB::commit();
        return redirect()->back()->with('msg', 'add Room success');
    }

    public function detailRoom($id)
    {
        $user = Auth::user();
        $detail = Property::find($id);
        $room = Room::where('property_id', $detail->id)->get();
        $this->data['user'] = $user;
        $this->data['room'] = $room;
        $this->data['detail'] = $detail;
        return view('owner.detail-property', $this->data);
    }

    public function editRoom(Request $request)
    {
        $user = Auth::user();
        $this->validate($request, [
            'name' => 'required',
            'price' => 'required',
            'facilities' => 'required'
        ]);

        $facilities = implode("|", $request->facilities);

        $room = Room::find($request->id);
        $room->name = $request->name;
        $room->price = $request->price;
        $room->facilities = $facilities;
        $room->save();

        return redirect()->back()->with('msg', 'Edit Room success');
    }

    public function deleteRoom(Request $request)
    {
        $user = Auth::user();
        
        DB::beginTransaction();
        try {
            

        $room = Room::find($request->room_id);
        $updateProperty = Property::find($room->property_id);
        $room->delete();
        $updateProperty->total_room -=1;
        $updateProperty->save();
        } catch (\Exception $e) {
            dd($e);
            DB::rollback();
            return redirect()->back()->with("msgerr", trans('Failed Delete Room'))->withInput();
        }
        DB::commit();
        return redirect()->back()->with('msg', 'delete Room success');
    }

    public function getRoom(Request $request)
    {
        $parent = $request->parent_id;

        $room = Room::where("id", $parent)->get();

        return response()->json($room->toArray())->setStatusCode(200);
    }
}
