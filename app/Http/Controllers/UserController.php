<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Facility;
class UserController extends Controller
{
    public $data;
    public function __construct()
    {
        $this->data['facility'] = Facility::all();
    }
    public function ownerRegistForm()
    {
        return view('auth.owner-register');
    }
    public function processRegister(Request $request)
    {

        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $newUser = new User();
        $newUser->name = $request->name;
        $newUser->email= $request->email;
        $newUser->password = Hash::make($request->password);
        $newUser->role = 'owner';
        $newUser->save();
        return redirect('login');
    }

    public function myAccountOwner()
    {
        // dd($this->data);
        $user = Auth::user();
        $this->data['user'] = $user;
        return view('owner.account',$this->data);
    }
}
