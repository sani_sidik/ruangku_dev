@extends('layouts.owner.app')
@section('content')
<!-- ############ Content START-->
            <div id="content" class="flex ">
                <!-- ############ Main START-->
                <div>
                    <div class="">
                        <div class="d-flex padding">
                            <div>
                                <h2 class="text-md text-highlight">
                                    My Property
                                </h2>
                                <small class="text-muted">{{date('l')}}, {{date("d M Y")}}</small>
                                <a class="badge badge-sm badge-pill b-a mx-1" id="todayview">Today</a>
                            </div>
                            <span class="flex"></span>
                            <div>
                                <button id="btn-new" class="btn btn-sm box-shadows btn-rounded gd-primary text-white">
                                    Add Property
                                </button>
                            </div>
                        </div>

                    </div>

                    <!-- modal -->
                    <div id="newEvent" class="modal fade">
                        <div class="modal-dialog text-dark">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <h5 class="modal-title text-white">New Property</h5>
                                    <button class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                <form method="POST" action="{{url('owner/add-property')}}" enctype="multipart/form-data">
                                    @csrf
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Title</label>
                                            <div class="col-sm-9">
                                                <input id="event-title" type="text" name="name" class="form-control" placeholder="Title">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Property type</label>
                                            <div class="col-sm-9">
                                                <div class="mt-2" id="event-type">
                                                    <div class="form-check form-check-inline">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio" name="type" value="kontrakan"> Kontrakan</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <label class="form-check-label">
                                                            <input class="form-check-input" type="radio" name="type" value="kos"> Kos</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Provience</label>
                                                <div class="col-sm-9">
                                                    <select name="provience_id" class="form-control">
                                                        <option value="5">DKI Jakarta</option>
                                                        <option>Kalimantan Selatan</option>
                                                        <option>Kalimantatn Tengah</option>
                                                        <option>Jawa Tengah</option>
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">City</label>
                                        <div class="col-sm-9">
                                            <select name="city_id" class="form-control">
                                                <option value="49">Jakarta Selatan</option>
                                                <option>Pangkalan Bun</option>
                                                <option>Tanjung</option>
                                                <option>Semarang</option>
                                            </select>
                                        </div>
                                        </div>
                                        <div class="form-group row row-sm">
                                            <label class="col-sm-3 col-form-label">Room</label>
                                            <div class="col-sm-5">
                                                <input  type="number" name="total_room" class="form-control" placeholder="Min 1 Max 100">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Addres</label>
                                            <div class="col-sm-9">
                                                <textarea  name="address" class="form-control" rows="6"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Description</label>
                                            <div class="col-sm-9">
                                                <textarea  name="description" class="form-control" rows="6"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3"></label>
                                            <div class="col-sm-9">
                                                <button type="submit" id="btn-save" class="btn gd-primary text-white btn-rounded">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                    </div>
                    <!-- / .modal -->

                    <!-- Js Call Modal man -->
                    <div data-plugin="fullCalendar">
                    </div>
                    <!-- Js Call Modal man -->


                    <!-- main content -->
                    <div class="page-content page-container" id="page-content">
                        <div class="padding">
                            <div class="row">
                            @foreach($property as $val)
                                <div class="col-sm-4">
                                    <div class="card">
                                        <div class="media media-2x1 gd-info">
                                            <a class="media-content" style="background-image:url()">
                                                <strong class="text-fade">Card media</strong>
                                            </a>
                                        </div>
                                        <a href="{{url('owner/property').'/'.$val->id}}">
                                        <div class="card-body">
                                            <h5 class="card-title">{{$val->name}}</h5>
                                            <p class="card-text">{{isset($val->description)?$val->description:"no Description"}}</p>
                                        </div>
                                        <div class="card-body">
                                        <div>Room Fill</div>
                                        <div class="progress no-bg mt-2 align-items-center circle" style="height:6px;">
                                            <div class="progress-bar circle gd-danger" style="width: 65%"></div>
                                            <span class="mx-2">65%</span>
                                        </div>
                                        </div>
                                        </a>
                                        <div class="card-body">
                                        <div class="item-action dropdown">
                                                    <a href="#" data-toggle="dropdown" class="text-muted">
                                                        <i data-feather="more-vertical"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-left bg-gray" role="menu">
                                                        <a class="dropdown-item" href="#">
                                                            Property detail
                                                        </a>
                                                        <a onclick="editProperty({{$val->id}})" class="dropdown-item edit">
                                                            Edit Property
                                                        </a>
                                                        <div  class="dropdown-divider"></div>
                                                        <a onclick="deleteProperty({{$val->id}})" class="dropdown-item">
                                                            Delete Property
                                                        </a>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- main content -->

                </div>
                <!-- ############ Main END-->
            </div>
            <!-- ############ Content END-->

            <!-- modal -->
            <div id="editForm" class="modal fade">
                        <div class="modal-dialog text-dark">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <h5 class="modal-title text-white">New Property</h5>
                                    <button id="close" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                <form method="POST" action="{{url('owner/edit-property')}}" enctype="multipart/form-data">
                                    @csrf
                                    <input id="hisId" type="hidden" name="id">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Title</label>
                                            <div class="col-sm-9">
                                                <input id="name" type="text" name="name" class="form-control" placeholder="Title">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Property type</label>
                                            <div class="col-sm-9">
                                                <div class="mt-2" id="event-type">
                                                    <div class="form-check form-check-inline">
                                                        <label class="form-check-label">
                                                            <input id="kontrakan" class="form-check-input" type="radio" name="type" value="kontrakan"> Kontrakan</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <label class="form-check-label">
                                                            <input id="kos" class="form-check-input" type="radio" name="type" value="kos"> Kos</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Provience</label>
                                                <div class="col-sm-9">
                                                    <select name="provience_id" class="form-control">
                                                        <option value="5">DKI Jakarta</option>
                                                        <option>Kalimantan Selatan</option>
                                                        <option>Kalimantatn Tengah</option>
                                                        <option>Jawa Tengah</option>
                                                    </select>
                                                </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">City</label>
                                        <div class="col-sm-9">
                                            <select name="city_id" class="form-control">
                                                <option value="49">Jakarta Selatan</option>
                                                <option>Pangkalan Bun</option>
                                                <option>Tanjung</option>
                                                <option>Semarang</option>
                                            </select>
                                        </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Addres</label>
                                            <div class="col-sm-9">
                                                <textarea id="address" name="address" class="form-control" rows="6"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Description</label>
                                            <div class="col-sm-9">
                                                <textarea id="description"  name="description" class="form-control" rows="6"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3"></label>
                                            <div class="col-sm-9">
                                                <button type="submit" class="btn gd-primary text-white btn-rounded">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                    </div>
                    <!-- / .modal -->
</div>
@endsection
@section('script')
<script>

</script>
@endsection