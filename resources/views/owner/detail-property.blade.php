@extends('layouts.owner.app')
@section('content')
            <!-- ############ Content START-->
            <div id="content" class="flex ">
                <!-- ############ Main START-->
                <div>
                    <div class="page-hero page-container " id="page-hero">
                    <div class="d-flex padding">
                            <div>
                                <h2 class="text-md text-highlight">
                                {{$detail->name}}
                                </h2>
                                <small class="text-muted">{{$detail->description}}</small>
                            </div>
                            <span class="flex"></span>
                            <div>
                                <button id="btn-new" class="btn btn-sm box-shadows btn-rounded gd-primary text-white">
                                    Add Room
                                </button>
                            </div>
                        </div>
                    </div>

                    <!-- modal -->
                    <div id="newEvent" class="modal fade">
                        <div class="modal-dialog text-dark">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <h5 class="modal-title text-white">New Room</h5>
                                    <button class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                <form method="POST" action="{{url('owner/add-room')}}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="property_id" value="{{$detail->id}}">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Name/Number</label>
                                            <div class="col-sm-9">
                                                <input id="event-title" type="text" name="name" class="form-control" placeholder="Title" require>
                                            </div>
                                        </div>

                                        <div class="form-group row row-sm">
                                            <label class="col-sm-3 col-form-label">Price(Rp)</label>
                                            
                                            <div class="col-sm-5">
                                                <input  type="number" name="price" class="form-control" placeholder="50000" require>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Facility</label>
                                            <div class="col-sm-5">
                                                @foreach($facility as $val)
                                                    <div class="checkbox">
                                                        <label class="ui-check">
                                                            <input type="checkbox" name="facilities[]" value="{{$val->name}}">
                                                            <i class="dark-white"></i>
                                                            {{$val->name}}
                                                        </label>
                                                    </div>
                                                @endforeach
                                                </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3"></label>
                                            <div class="col-sm-9">
                                                <button type="submit" id="btn-save" class="btn gd-primary text-white btn-rounded">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                    </div>
                    <!-- / .modal -->
                    <!-- Js Call Modal man -->
                    <div data-plugin="fullCalendar">
                    </div>
                    <!-- Js Call Modal man -->

                    <div class="page-content page-container" id="page-content">
                        <div class="padding">
                            <div class="table-responsive">
                                <table id="datatable" class="table table-theme table-row v-middle" data-plugin="dataTable">
                                    <thead>
                                        <tr>
                                            <th><span class="text-muted">Status</span></th>
                                            <th><span class="text-muted">Price</span></th>
                                            <th><span class="text-muted">RoomName/RoomRental</span></th>
                                            <th><span class="text-muted d-none d-sm-block">Start</span></th>
                                            <th><span class="text-muted d-none d-sm-block">End</span></th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($room as $val)
                                        <tr class=" " data-id="11">
                                            <td>
                                                <a href="#">
                                                    @if($val->status == 0)
                                                    <span class="w-32 avatar gd-info">
                                                    <i class="fas fa-check"></i>
                                                    </span>
                                                    @else
                                                    <span class="w-32 avatar gd-danger">
                                                    <i class="fas fa-times"></i>
                                                    </span>
                                                    @endif
                                                </a>
                                            </td>
                                            <td style="">
                                                <small class="text-muted">Rp {{$val->price}}</small>
                                            </td>
                                            <td class="flex">
                                                <a href="#" class="item-title text-color ">{{isset($val->name)?$val->name: "Unamed"}}</a>
                                                <div class="item-except text-muted text-sm h-1x">
                                                @if($val->status == 0) Ready @else Book/On @endif
                                                </div>
                                            </td>
                                            <td>
                                                <span class="item-amount d-none d-sm-block text-sm ">
                                                200
                                                </span>
                                            </td>
                                            <td>
                                                <span class="item-amount d-none d-sm-block text-sm [object Object]">
                                                53
                                                </span>
                                            </td>
                                            <td>
                                                <div class="item-action dropdown">
                                                    <a href="#" data-toggle="dropdown" class="text-muted">
                                                        <i data-feather="more-vertical"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right bg-black" role="menu">
                                                        <a class="dropdown-item" href="#">
                                                            Room detail
                                                        </a>
                                                        <a onclick="editRoom({{$val->id}})" class="dropdown-item edit">
                                                            Edit Room
                                                        </a>
                                                        <div  class="dropdown-divider"></div>
                                                        <a onclick="deleteRoom({{$val->id}})" class="dropdown-item">
                                                            Delete Room
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <!-- ############ Main END-->
            </div>
            <!-- ############ Content END-->

            <!-- modal -->
            <div id="editForm" class="modal fade">
                        <div class="modal-dialog text-dark">
                            <div class="modal-content">
                                <div class="modal-header bg-primary">
                                    <h5 class="modal-title text-white">Edit Room</h5>
                                    <button id="close" class="close" >&times;</button>
                                </div>
                                <div class="modal-body">
                                <form method="POST" action="{{url('owner/edit-room')}}" enctype="multipart/form-data">
                                    @csrf
                                    <input id="hisId" type="hidden" name="id" >
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Name/Number</label>
                                            <div class="col-sm-9">
                                                <input id="name" type="text" name="name" class="form-control" placeholder="Title">
                                            </div>
                                        </div>

                                        <div class="form-group row row-sm">
                                            <label class="col-sm-3 col-form-label">Price(Rp)</label>
                                            
                                            <div class="col-sm-5">
                                                <input id="price"  type="number" name="price" class="form-control" placeholder="50000">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Facility</label>
                                            <div class="col-sm-5">
                                                @foreach($facility as $val)
                                                    <div class="checkbox">
                                                        <label class="ui-check">
                                                            <input type="checkbox" name="facilities[]" value="{{$val->name}}">
                                                            <i class="dark-white"></i>
                                                            {{$val->name}}
                                                        </label>
                                                    </div>
                                                @endforeach
                                                </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3"></label>
                                            <div class="col-sm-9">
                                                <button type="submit" class="btn gd-primary text-white btn-rounded">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        <!-- /.modal-content -->
                        </div>
                    </div>
@endsection
@section('script')
<script>


</script>
@endsection