-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 15, 2020 at 04:39 AM
-- Server version: 10.3.14-MariaDB
-- PHP Version: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tamsat_komen`
--

-- --------------------------------------------------------

--
-- Table structure for table `billings`
--

CREATE TABLE `billings` (
  `id` int(10) UNSIGNED NOT NULL,
  `owner_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `tanggal_bayar` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(10) UNSIGNED NOT NULL,
  `kabupaten` varchar(225) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provience_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `kabupaten`, `city_name`, `provience_id`) VALUES
(1, 'Kabupaten Aceh Barat', 'Meulaboh', 1),
(2, 'Kabupaten Aceh Barat Daya', 'Blangpidie', 1),
(3, 'Kabupaten Aceh Besar', 'Kota Jantho', 1),
(4, 'Kabupaten Aceh Selatan', 'Tapak Tuan', 1),
(5, 'Kabupaten Aceh Jaya', 'Calang', 1),
(6, 'Kabupaten Aceh Singkil', 'Singkil', 1),
(7, 'Kabupaten Aceh Tamiang', 'Karang Baru', 1),
(8, 'Kabupaten Aceh Tengah', 'Takengon', 1),
(9, 'Kabupaten Aceh Tenggara', 'Kutacane', 1),
(10, 'Kabupaten Aceh Timur', 'Idi Rayeuk', 1),
(11, 'Kabupaten Aceh Utara', 'Lhoksukon', 1),
(12, 'Kabupaten Bener Meriah', 'Simpang Tiga Redelong', 1),
(13, 'Kabupaten Bireun', 'Bireun', 1),
(14, 'Kabupaten Gayo Lues', 'Blang Kejeren', 1),
(15, 'Kabupaten Nagan Raya', 'Suka Makmue', 1),
(16, 'Kabupaten Pidie', 'Sigli', 1),
(17, 'Kabupaten Pidie jaya', 'Meureudu', 1),
(18, 'Kabupaten Simeulue', 'Sinabang', 1),
(19, 'Kota Banda Aceh', 'Kota Banda Aceh', 1),
(20, 'Kota Langsa', 'Kota Langsa', 1),
(21, 'Kota Lhokseumawe', 'Kota Lhokseumawe', 1),
(22, 'Kota Sabang', 'Kota Sabang', 1),
(23, 'Kota Subulussalam', 'Kota Subulussalam', 1),
(24, 'Kabupaten Lebak', 'Rangkasbitung', 2),
(25, 'Kabupaten Pandeglang', 'Pandeglang', 2),
(26, 'Kabupaten Serang', 'Ciruas', 2),
(27, 'Kabupaten Tangerang', 'Tigaraksa', 2),
(28, 'Kota Cilegon', 'Kota Cilegon', 2),
(29, 'Kota Serang', 'Kota Serang', 2),
(30, 'Kota Tangerang', 'Kota Tangerang', 2),
(31, 'Kota Tangerang Selatan', 'Kota Tangerang Selatan', 2),
(32, 'Kabupaten Bengkulu Selatan', 'Kota Mana', 3),
(33, 'Kabupaten Bengkulu Tengah', 'Karang Tinggi', 3),
(34, 'Kabupaten Bengku Utara', 'Arga Makmur', 3),
(35, 'Kabupaten Kepahiang', 'Kepahiang', 3),
(36, 'Kabupaten Mukomuko', 'Mukomuko', 3),
(37, 'Kabupaten Rejang Lebong', 'Curup', 3),
(38, 'Kabupaten Seluma', 'Tais', 3),
(39, 'Kota Bengkulu', 'Kota Bengkulu', 3),
(40, 'Kabupaten Boalemo', 'Tilamuta', 4),
(41, 'Kabupaten Bone Bolango', 'Suwawa', 4),
(42, 'Kabupaten Gorontalo', 'Limboto', 4),
(43, 'Kabupaten Gorontalo Utara', 'Kwandang', 4),
(44, 'Kabupaten Pohuwato', 'Marisa', 4),
(45, 'Kota Gorontalo', 'Kota Gorontalo', 4),
(46, 'Kepulauan Seribu', 'Pulau Pramuka', 5),
(47, 'Jakarta Barat', 'Kembangan', 5),
(48, 'Jakarta Pusat', 'Menteng', 5),
(49, 'Jakarta Selatan', 'Kebayoran Baru', 5),
(50, 'Jakarta Timur', 'Cakung', 5),
(51, 'Jakarta Utara', 'Koja', 5);

-- --------------------------------------------------------

--
-- Table structure for table `facilities`
--

CREATE TABLE `facilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `facilities`
--

INSERT INTO `facilities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Kamar Mandi Dalam', NULL, NULL),
(2, 'AC', NULL, NULL),
(3, 'Kipas Angin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_12_26_073020_table_owner', 1),
(4, '2019_12_26_073121_table_city', 2),
(5, '2019_12_26_073138_table_provinces', 2),
(6, '2019_12_26_073152_table_room', 2),
(7, '2019_12_26_073200_table_billing', 2);

-- --------------------------------------------------------

--
-- Table structure for table `owners`
--

CREATE TABLE `owners` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE `property` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `owner_id` bigint(20) NOT NULL,
  `city_id` int(10) NOT NULL,
  `total_room` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `property`
--

INSERT INTO `property` (`id`, `name`, `address`, `owner_id`, `city_id`, `total_room`, `type`, `description`, `created_at`, `updated_at`) VALUES
(8, 'weqweqwe', 'qweqweq', 2, 49, '1', 'kos', 'qeqeqweqw', '2020-01-06 14:51:24', '2020-01-06 15:57:46'),
(9, 'tes', 'qwe', 2, 49, '3', 'kos', 'qweqwe', '2020-01-06 14:51:35', '2020-01-06 14:51:35'),
(10, 'tes', 'qweqweqw', 2, 49, '3', 'kontrakan', 'qweqweqw', '2020-01-06 14:51:46', '2020-01-06 14:51:46'),
(12, 'asdsd', 'sadasd', 5, 49, '2', 'kontrakan', 'asdasd', '2020-01-07 14:09:14', '2020-01-07 14:09:14');

-- --------------------------------------------------------

--
-- Table structure for table `proviences`
--

CREATE TABLE `proviences` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ibu_kota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `proviences`
--

INSERT INTO `proviences` (`id`, `name`, `ibu_kota`) VALUES
(1, 'Aceh', 'Banda Aceh'),
(2, 'Banten', 'Serang'),
(3, 'Bengkulu', 'Bengkulu'),
(4, 'Gorontalo', 'Gorontalo'),
(5, 'Jakarta', 'Jakarta'),
(6, 'Jambi', 'Jambi'),
(7, 'Jawa Barat', 'Bandung'),
(8, 'Jawa Tengah', 'Semarang'),
(9, 'Jawa Timur', 'Surabaya'),
(10, 'Kalimantan Barat', 'Pontianak'),
(11, 'Kalimantan Selatan', 'Banjarmasin'),
(12, 'Kalimantan Tengah', 'Palangka Raya'),
(13, 'Kalimantan Timur', 'Samarinda'),
(14, 'Kalimantan Utara', 'Tanjung Selor'),
(15, 'Kepulauan Bangka Belitung', 'Pangkal Pinang'),
(16, 'Kepulauan Riau', 'Tanjungpinang'),
(17, 'Lampung', 'Bandar Lampung'),
(18, 'Maluku', 'Ambon'),
(19, 'Maluku Utara', 'Sofifi'),
(20, 'Bali', 'Denpasar'),
(21, 'Nusa Tenggara Barat', 'Mataram'),
(22, 'Nusa Tenggara Timur', 'Kupang'),
(23, 'Papua', 'Jayapura'),
(24, 'Papua Barat', 'Manokwari'),
(25, 'Riau', 'Pekanbaru'),
(26, 'Sulawesi Barat', 'Mamuju'),
(27, 'Sulawesi Selatan', 'Makassar'),
(28, 'Sulawesi Tengah', 'Palu'),
(29, 'Sulawesi Tenggara', 'Kendari'),
(30, 'Sumatra Barat', 'Padang'),
(31, 'Sumatra Selatan', 'Palembang'),
(32, 'Sumatra Utara', 'Medan'),
(33, 'Yogyakarta', 'Yogyakarta');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `property_id` int(11) NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '50000',
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0 = kosong, 1 = isi, 2=book',
  `facilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `name`, `property_id`, `price`, `status`, `facilities`, `created_at`, `updated_at`) VALUES
(18, NULL, 8, '50000', 0, NULL, '2020-01-06 14:51:24', '2020-01-06 14:51:24'),
(19, NULL, 9, '50000', 0, NULL, '2020-01-06 14:51:35', '2020-01-06 14:51:35'),
(20, NULL, 9, '50000', 0, NULL, '2020-01-06 14:51:35', '2020-01-06 14:51:35'),
(21, NULL, 9, '50000', 0, NULL, '2020-01-06 14:51:35', '2020-01-06 14:51:35'),
(22, NULL, 10, '50000', 0, NULL, '2020-01-06 14:51:46', '2020-01-06 14:51:46'),
(23, NULL, 10, '50000', 0, NULL, '2020-01-06 14:51:46', '2020-01-06 14:51:46'),
(24, NULL, 10, '50000', 0, NULL, '2020-01-06 14:51:46', '2020-01-06 14:51:46'),
(31, 'ewrewr', 12, '50000', 0, 'Kamar Mandi Dalam', '2020-01-07 14:09:14', '2020-01-07 14:09:37'),
(32, NULL, 12, '50000', 0, NULL, '2020-01-07 14:09:14', '2020-01-07 14:09:14');

-- --------------------------------------------------------

--
-- Table structure for table `tbladdons`
--

CREATE TABLE `tbladdons` (
  `id` int(10) NOT NULL,
  `packages` text NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `billingcycle` text NOT NULL,
  `tax` tinyint(1) NOT NULL,
  `showorder` tinyint(1) NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT 0,
  `retired` tinyint(1) NOT NULL DEFAULT 0,
  `downloads` text NOT NULL,
  `autoactivate` text NOT NULL,
  `suspendproduct` tinyint(1) NOT NULL,
  `welcomeemail` int(10) NOT NULL,
  `type` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `module` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `server_group_id` int(10) NOT NULL DEFAULT 0,
  `weight` int(2) NOT NULL DEFAULT 0,
  `autolinkby` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbladdons`
--

INSERT INTO `tbladdons` (`id`, `packages`, `name`, `description`, `billingcycle`, `tax`, `showorder`, `hidden`, `retired`, `downloads`, `autoactivate`, `suspendproduct`, `welcomeemail`, `type`, `module`, `server_group_id`, `weight`, `autolinkby`, `created_at`, `updated_at`) VALUES
(1, '', 'Positive SSL', 'Sertifikat SSL dari Positive yang bisa digunakan untuk domain anda.', 'Annually', 0, 1, 0, 0, '', 'payment', 0, 0, '', '', 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, '', 'Comodo SSL', 'Sertifiktat SSL dari Comodo yang bisa digunakan untuk mengamankan domain anda.', 'Annually', 0, 0, 0, 0, '', 'payment', 0, 0, 'hostingaccount', '', 0, 0, '', '0000-00-00 00:00:00', '2018-12-07 09:08:40'),
(3, '11,126,125,130,129,128,127,132,131,121,136,135,137', 'Dedicated IP', 'Ip yang anda dapat berbeda dengan ip pengguna lain, berguna bagi anda yang sering mengirim email, atau untuk keperluan lain', 'recurring', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '0000-00-00 00:00:00', '2020-01-18 10:27:32'),
(35, '40', 'Backup Neutron', '', 'Monthly', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2020-01-13 03:53:20', '2020-01-13 03:56:30'),
(5, '', 'Managed Service (CMS, Wordpress, Drupal, Magento)', 'Kita akan me-managed server anda. Pilihan ini untuk anda yang menggunakan CMS di website, seperti Wordpress, Drupal, Magento, Prestashop dan lainnya.', 'Monthly', 0, 0, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '0000-00-00 00:00:00', '2018-09-08 14:23:14'),
(6, '43,44', 'Managed Server (Custom + cPanel)', 'Kita akan me-managed server anda.  Pilihan ini tepat bagi anda yang menggunakan custom script, juga sudah termasuk license cPanel.', 'Monthly', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '0000-00-00 00:00:00', '2018-03-27 04:14:43'),
(8, '4,5,6,1,2,3,11,15,13,16,28,29', 'Pindah Hosting US-ID atau ID-US', 'Pindah Hosting US-ID atau ID-US', 'onetime', 0, 0, 1, 1, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '0000-00-00 00:00:00', '2020-05-04 02:47:55'),
(9, '39,40,41,42,81,82,83,84', 'cPanel/WHM 5 Akun untuk VPS', 'cPanel/WHM 5 Akun untuk VPS', 'Monthly', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '0000-00-00 00:00:00', '2020-03-24 10:28:26'),
(10, '', 'Email Premium', 'Pastikan Email Anda Mendarat di Inbox Penerima.\r\nLayanan email premium dari Jetorbit.\r\n30.000 email keluar perbulan dengan biaya Rp 50.000/bulan/domain', 'recurring', 0, 0, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2018-02-26 08:30:28', '2018-09-08 14:29:29'),
(11, '179,178,177,52,118,112,113,104,107,105,106,4,53,5,6,54,101,120,141,144,49,1,50,2,51,3,117,86,85,103,114,100,142,147,145,174,146,158,160,17,7,55,11,126,125,122,56,15,130,129,57,13,128,127,58,16,132,131,121,136,135,137,65,35,66,36,67,37,68,38,45,46,47,110,111,108,109,28,29,78,167,116,124,164,134,138,139,140', 'Email Spam Filtering - Incoming Scanning', 'SpamExperts Incoming Filtering software offers full inbox protection against spam, viruses and malware with the help of advanced algorithms and spam pattern detection methods.', 'recurring', 1, 0, 1, 0, '', 'payment', 0, 0, '', 'marketconnect', 0, 201, '{\"type\":[\"hostingaccount\",\"reselleraccount\"]}', '2018-03-20 04:32:48', '2020-05-15 00:42:16'),
(12, '11,55,108,109,110,111,121,147,177,178,179,1,49,78,116,124,126,134,138,145,7,17,35,36,37,38,52,65,66,67,68,120,125,139,167,174,2,28,29,50,118,122,140,146,45,46,47,112,158,3,15,51,56,113,160,104,136,107,117,130,85,86,105,129,13,57,103,106,135,4,53,128,5,114,127,6,16,58,54,100,137,101,132,141,142,131,144,164', 'Email Spam Filtering - Outgoing Scanning', 'Outgoing Email Filtering prevents IPs from getting blacklisted as the result of spam unknowingly being sent out through a compromised account, offering full network protection and IP reputation.', 'recurring', 1, 0, 1, 0, '', 'payment', 0, 0, '', 'marketconnect', 0, 202, '{\"type\":[\"hostingaccount\",\"reselleraccount\"]}', '2018-03-20 04:32:48', '2020-05-15 00:42:16'),
(13, '11,55,108,109,110,111,121,147,177,178,179,1,49,78,116,124,126,134,138,145,7,17,35,36,37,38,52,65,66,67,68,120,125,139,167,174,2,28,29,50,118,122,140,146,45,46,47,112,158,3,15,51,56,113,160,104,136,107,117,130,85,86,105,129,13,57,103,106,135,4,53,128,5,114,127,6,16,58,54,100,137,101,132,141,142,131,144,164', 'Email Spam Filtering - Email Archiving', 'The Email Archiving solution offers bulk storage licenses or archiving units based on storage (GB) requirements, to  securely archive email for back-up and compliance purposes.', 'recurring', 1, 0, 1, 0, '', 'payment', 0, 0, '', 'marketconnect', 0, 203, '{\"type\":[\"hostingaccount\",\"reselleraccount\"]}', '2018-03-20 04:32:48', '2020-05-15 00:42:16'),
(14, '11,55,108,109,110,111,121,147,177,178,179,1,49,78,116,124,126,134,138,145,7,17,35,36,37,38,52,65,66,67,68,120,125,139,167,174,2,28,29,50,118,122,140,146,45,46,47,112,158,3,15,51,56,113,160,104,136,107,117,130,85,86,105,129,13,57,103,106,135,4,53,128,5,114,127,6,16,58,54,100,137,101,132,141,142,131,144,164', 'Email Spam Filtering - Incoming &amp; Archiving Bundle', 'SpamExperts Incoming Filtering and Email Archiving bundle offers full inbox protection against spam, viruses, malware, and bulk storage licenses or archiving units based on storage (GB) requirements, to securely archive email for back-up and compliance purposes.', 'recurring', 1, 0, 1, 0, '', 'payment', 0, 0, '', 'marketconnect', 0, 204, '{\"type\":[\"hostingaccount\",\"reselleraccount\"]}', '2018-03-20 04:32:48', '2020-05-15 00:42:16'),
(15, '11,55,108,109,110,111,121,147,177,178,179,1,49,78,116,124,126,134,138,145,7,17,35,36,37,38,52,65,66,67,68,120,125,139,167,174,2,28,29,50,118,122,140,146,45,46,47,112,158,3,15,51,56,113,160,104,136,107,117,130,85,86,105,129,13,57,103,106,135,4,53,128,5,114,127,6,16,58,54,100,137,101,132,141,142,131,144,164', 'Email Spam Filtering - Outgoing &amp; Archiving Bundle', 'SpamExperts Outgoing Filtering and Email Archiving bundle prevent IPs from getting blacklisted offering full network, IP reputation protection, and bulk storage licenses or archiving units based on storage (GB) requirements, to securely archive email for back-up and compliance purposes.', 'recurring', 1, 0, 1, 0, '', 'payment', 0, 0, '', 'marketconnect', 0, 205, '{\"type\":[\"hostingaccount\",\"reselleraccount\"]}', '2018-03-20 04:32:48', '2020-05-15 00:42:16'),
(16, '11,55,108,109,110,111,121,147,177,178,179,1,49,78,116,124,126,134,138,145,7,17,35,36,37,38,52,65,66,67,68,120,125,139,167,174,2,28,29,50,118,122,140,146,45,46,47,112,158,3,15,51,56,113,160,104,136,107,117,130,85,86,105,129,13,57,103,106,135,4,53,128,5,114,127,6,16,58,54,100,137,101,132,141,142,131,144,164', 'Email Spam Filtering - Incoming &amp; Outgoing Bundle', 'SpamExperts Incoming and Outgoing Filtering offers full inbox protection against spam, viruses and malware and prevents IPs from getting blacklisted offering full network and IP reputation protection.', 'recurring', 1, 0, 1, 0, '', 'payment', 0, 0, '', 'marketconnect', 0, 206, '{\"type\":[\"hostingaccount\",\"reselleraccount\"]}', '2018-03-20 04:32:48', '2020-05-15 00:42:16'),
(17, '11,55,108,109,110,111,121,147,177,178,179,1,49,78,116,124,126,134,138,145,7,17,35,36,37,38,52,65,66,67,68,120,125,139,167,174,2,28,29,50,118,122,140,146,45,46,47,112,158,3,15,51,56,113,160,104,136,107,117,130,85,86,105,129,13,57,103,106,135,4,53,128,5,114,127,6,16,58,54,100,137,101,132,141,142,131,144,164', 'Email Spam Filtering - Incoming, Outgoing &amp; Archiving Bundle', 'The complete suite of email security offering Incoming and Outgoing full inbox protection, along with Archiving to ensure you never lose another email. Get the highest level of network and IP reputation protection and assured integrity and continuity for emails.', 'recurring', 1, 0, 1, 0, '', 'payment', 0, 0, '', 'marketconnect', 0, 207, '{\"type\":[\"hostingaccount\",\"reselleraccount\"]}', '2018-03-20 04:32:48', '2020-05-15 00:42:16'),
(18, '', 'SSL Certificates - RapidSSL', 'RapidSSL® Certificates help you keep your customers\' transactions secure with up to 256-bit data encryption and site authentication. Buy a certificate and speed through enrollment with automated domain control validation. Best of all, our SSL certificates are among the most affordable in the industry. We keep our costs down and pass the savings on to you.', 'recurring', 1, 0, 0, 0, '', 'payment', 0, 0, '', 'marketconnect', 0, 1, '{\"type\":[\"hostingaccount\",\"reselleraccount\",\"server\"]}', '2018-09-28 07:22:04', '2020-01-14 09:19:19'),
(39, '168,171,148', 'Managed Server', '', 'Monthly', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2020-02-04 06:29:37', '2020-03-05 05:33:35'),
(40, '22,39,40,41,42,81,82,83,84,45,168,173,171,148', 'Cpanel/WHM 100 Akun Dedicated Server/VPS', 'Cpanel/WHM 100 Akun Dedicated Server/VPS', 'Monthly', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2020-03-19 07:08:38', '2020-03-24 10:29:15'),
(19, '11,23,27,39,40,41,42,43,44,55,69,108,109,110,111,121,1,49,52,78,81,116,124,126,134,138,7,17,35,36,37,38,65,66,67,68,82,120,125,139,147,2,28,29,50,83,87,118,122,140,145,45,46,47,84,123,3,15,51,56,102,146,112,136,150,113,117,130,143,85,86,104,129,13,57,103,107,135,105,106,114,128,4,100,127,16,53,58,142,137,141,5,132,6,131,54,133,144,101,148,149', 'SSL Certificates - RapidSSL Wildcard', 'RapidSSL® Wildcard Certificates help you secure multiple subdomains with one low-cost SSL certificate issued to *.yourdomain.com. Protect your customers\' personal data with up to 256-bit encryption. Automated domain control validation makes site authentication a breeze. At RapidSSL, we believe in providing the SSL protection you need at a competitive price.', 'recurring', 1, 0, 0, 0, '', 'payment', 0, 0, 'other', 'marketconnect', 0, 2, '{\"type\":[\"hostingaccount\",\"reselleraccount\",\"server\"]}', '2018-09-28 07:22:04', '2019-10-25 07:50:04'),
(20, '11,23,27,39,40,41,42,43,44,55,69,108,109,110,111,121,1,49,52,78,81,116,124,126,134,138,7,17,35,36,37,38,65,66,67,68,82,120,125,139,147,2,28,29,50,83,87,118,122,140,145,45,46,47,84,123,3,15,51,56,102,146,112,136,150,113,117,130,143,85,86,104,129,13,57,103,107,135,105,106,114,128,4,100,127,16,53,58,142,137,141,5,132,6,131,54,133,144,101,148,149', 'SSL Certificates - GeoTrust QuickSSL Premium', 'Secure your website fast. GeoTrust® QuickSSL® Premium certificates are one of the quickest ways for you to start protecting online transactions and applications with SSL. With an automatic authentication and issuance process, it takes just minutes to get your QuickSSL Premium SSL certificate. After that, managing and renewing your certificates is a snap. With GeoTrust, you get inexpensive SSL without sacrificing convenience, choice, or reliability.', 'recurring', 1, 0, 0, 0, '', 'payment', 0, 0, 'other', 'marketconnect', 0, 3, '{\"type\":[\"hostingaccount\",\"reselleraccount\",\"server\"]}', '2018-09-28 07:22:04', '2019-10-25 07:50:04'),
(21, '11,23,27,39,40,41,42,43,44,55,69,108,109,110,111,121,1,49,52,78,81,116,124,126,134,138,7,17,35,36,37,38,65,66,67,68,82,120,125,139,147,2,28,29,50,83,87,118,122,140,145,45,46,47,84,123,3,15,51,56,102,146,112,136,150,113,117,130,143,85,86,104,129,13,57,103,107,135,105,106,114,128,4,100,127,16,53,58,142,137,141,5,132,6,131,54,133,144,101,148,149', 'SSL Certificates - GeoTrust QuickSSL Premium Wildcard', 'Secure your domain and all same level subdomains fast. GeoTrust QuickSSL Premium Wildcard certificates are one of the quickest ways to start protecting all your subdomains on a single certificate. Our automated email authentication process means you get your certificate in minutes. With QuickSSL Premium wildcard certificates, you get unlimited subdomains and on an unlimited number of servers - one certificate that will adapt as your business grows.', 'recurring', 1, 0, 0, 0, '', 'payment', 0, 0, 'other', 'marketconnect', 0, 4, '{\"type\":[\"hostingaccount\",\"reselleraccount\",\"server\"]}', '2018-09-28 07:22:04', '2019-10-25 07:50:04'),
(22, '11,23,27,39,40,41,42,43,44,55,69,108,109,110,111,121,1,49,52,78,81,116,124,126,134,138,7,17,35,36,37,38,65,66,67,68,82,120,125,139,147,2,28,29,50,83,87,118,122,140,145,45,46,47,84,123,3,15,51,56,102,146,112,136,150,113,117,130,143,85,86,104,129,13,57,103,107,135,105,106,114,128,4,100,127,16,53,58,142,137,141,5,132,6,131,54,133,144,101,148,149', 'SSL Certificates - GeoTrust True BusinessID', 'Business-class SSL—only better. When it comes to selecting SSL security for your business, you have a lot of choices. With GeoTrust, the decision is easy. GeoTrust® True BusinessID SSL certificates let your customers know that your site is trustworthy and that you take their security seriously enough to get your certificate from a globally trusted certificate authority.', 'recurring', 1, 0, 0, 0, '', 'payment', 0, 0, 'other', 'marketconnect', 0, 5, '{\"type\":[\"hostingaccount\",\"reselleraccount\",\"server\"]}', '2018-09-28 07:22:04', '2019-10-25 07:50:04'),
(23, '11,23,27,39,40,41,42,43,44,55,69,108,109,110,111,121,1,49,52,78,81,116,124,126,134,138,7,17,35,36,37,38,65,66,67,68,82,120,125,139,147,2,28,29,50,83,87,118,122,140,145,45,46,47,84,123,3,15,51,56,102,146,112,136,150,113,117,130,143,85,86,104,129,13,57,103,107,135,105,106,114,128,4,100,127,16,53,58,142,137,141,5,132,6,131,54,133,144,101,148,149', 'SSL Certificates - GeoTrust True Business ID with EV', 'Make a strong security statement with the green address bar. GeoTrust® True BusinessID with EV (Extended Validation) is our premium business-class SSL security product, visually confirming the highest level of authentication available among SSL certificates. The green bar says it all. Web site visitors really notice when the address bar turns green in their high-security browsers and the organization field starts to rotate between your business name and GeoTrust.', 'recurring', 1, 0, 0, 0, '', 'payment', 0, 0, 'other', 'marketconnect', 0, 6, '{\"type\":[\"hostingaccount\",\"reselleraccount\",\"server\"]}', '2018-09-28 07:22:04', '2019-10-25 07:50:04'),
(24, '11,23,27,39,40,41,42,43,44,55,69,108,109,110,111,121,1,49,52,78,81,116,124,126,134,138,7,17,35,36,37,38,65,66,67,68,82,120,125,139,147,2,28,29,50,83,87,118,122,140,145,45,46,47,84,123,3,15,51,56,102,146,112,136,150,113,117,130,143,85,86,104,129,13,57,103,107,135,105,106,114,128,4,100,127,16,53,58,142,137,141,5,132,6,131,54,133,144,101,148,149', 'SSL Certificates - GeoTrust True Business ID Wildcard', 'Simplify certificate management with the convenience of a Wildcard certificate. GeoTrust® True BusinessID Wildcard certificates allow you to secure multiple subdomains on a single certificate, reducing management time and cost. Whether it\'s your company\'s home page or your mail server\'s hostname, protecting sensitive information is your goal — and it\'s our goal too.', 'recurring', 1, 0, 0, 0, '', 'payment', 0, 0, 'other', 'marketconnect', 0, 7, '{\"type\":[\"hostingaccount\",\"reselleraccount\",\"server\"]}', '2018-09-28 07:22:04', '2019-10-25 07:50:04'),
(25, '11,23,27,39,40,41,42,43,44,55,69,108,109,110,111,121,1,49,52,78,81,116,124,126,134,138,7,17,35,36,37,38,65,66,67,68,82,120,125,139,147,2,28,29,50,83,87,118,122,140,145,45,46,47,84,123,3,15,51,56,102,146,112,136,150,113,117,130,143,85,86,104,129,13,57,103,107,135,105,106,114,128,4,100,127,16,53,58,142,137,141,5,132,6,131,54,133,144,101,148,149', 'SSL Certificates - Symantec Secure Site', 'Get cost-effective protection and industry defining strength. From the first name in security.Protect your non-transactional and/or internal sites with up to 256-bit encryption. Without spending any more than you have to.', 'recurring', 1, 0, 0, 0, '', 'payment', 0, 0, 'other', 'marketconnect', 0, 8, '{\"type\":[\"hostingaccount\",\"reselleraccount\",\"server\"]}', '2018-09-28 07:22:04', '2019-10-25 07:50:04'),
(26, '11,23,27,39,40,41,42,43,44,55,69,108,109,110,111,121,1,49,52,78,81,116,124,126,134,138,7,17,35,36,37,38,65,66,67,68,82,120,125,139,147,2,28,29,50,83,87,118,122,140,145,45,46,47,84,123,3,15,51,56,102,146,112,136,150,113,117,130,143,85,86,104,129,13,57,103,107,135,105,106,114,128,4,100,127,16,53,58,142,137,141,5,132,6,131,54,133,144,101,148,149', 'SSL Certificates - Symantec Secure Site Pro', 'Strengthen your entire site\'s security with ECC encryption and proactive vulnerability assessment. Protect your transactions, data and communications with ECC, RSA and DSA algorithms for the strongest encryption you can get.', 'recurring', 1, 0, 0, 0, '', 'payment', 0, 0, 'other', 'marketconnect', 0, 9, '{\"type\":[\"hostingaccount\",\"reselleraccount\",\"server\"]}', '2018-09-28 07:22:04', '2019-10-25 07:50:04'),
(27, '11,23,27,39,40,41,42,43,44,55,69,108,109,110,111,121,1,49,52,78,81,116,124,126,134,138,7,17,35,36,37,38,65,66,67,68,82,120,125,139,147,2,28,29,50,83,87,118,122,140,145,45,46,47,84,123,3,15,51,56,102,146,112,136,150,113,117,130,143,85,86,104,129,13,57,103,107,135,105,106,114,128,4,100,127,16,53,58,142,137,141,5,132,6,131,54,133,144,101,148,149', 'SSL Certificates - Symantec Secure Site EV', 'Add the power of assurance to your sites and security, with visual reminders that every second of every visit is protected. Give your visitors visual assurance that their transactions are protected with RSA and DSA algorithms. All with one certificate.', 'recurring', 1, 0, 0, 0, '', 'payment', 0, 0, 'other', 'marketconnect', 0, 10, '{\"type\":[\"hostingaccount\",\"reselleraccount\",\"server\"]}', '2018-09-28 07:22:04', '2019-10-25 07:50:04'),
(28, '11,23,27,39,40,41,42,43,44,55,69,108,109,110,111,121,1,49,52,78,81,116,124,126,134,138,7,17,35,36,37,38,65,66,67,68,82,120,125,139,147,2,28,29,50,83,87,118,122,140,145,45,46,47,84,123,3,15,51,56,102,146,112,136,150,113,117,130,143,85,86,104,129,13,57,103,107,135,105,106,114,128,4,100,127,16,53,58,142,137,141,5,132,6,131,54,133,144,101,148,149', 'SSL Certificates - Symantec Secure Site Pro EV', 'Combine the most advanced encryption algorithm with visual assurance cues, for confidence without question. Protect your customers with up to 256-bit encryption and 3 algorithms—while providing visual proof that they’re really on your site.', 'recurring', 1, 0, 0, 0, '', 'payment', 0, 0, 'other', 'marketconnect', 0, 11, '{\"type\":[\"hostingaccount\",\"reselleraccount\",\"server\"]}', '2018-09-28 07:22:04', '2019-10-25 07:50:04'),
(29, '', 'cPanel/WHM 1 Akun Untuk VPS', 'cPanel/WHM 1 Akun Untuk VPS', 'Monthly', 0, 0, 1, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2019-08-14 02:19:57', '2020-05-06 04:43:54'),
(30, '39,40,41,42,81,82,83,84', 'cPanel/WHM 30 Akun untuk VPS', 'cPanel/WHM 30 Akun untuk VPS', 'Monthly', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2019-09-30 10:02:54', '2020-03-19 06:57:59'),
(31, '39,40,41,42,81,82,83,87,84,102,150,69', 'Plesk Web Admin Edition', ' 10 domains\r\n WordPress Toolkit SE', 'recurring', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2019-11-26 08:57:01', '2019-11-27 04:58:45'),
(32, '39,40,41,42,81,82,83,87,84,102,150,69', 'Plesk Web Pro Edition', '30 domains\r\nWordPress Toolkit\r\nDeveloper Pack\r\nSubscription Management\r\nAccount Management', 'recurring', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2019-11-26 09:01:09', '2019-11-27 04:59:10'),
(33, '39,40,41,42,81,82,83,87,84,102,150,69', 'Plesk Web Host Edition', 'Unlimited domains\r\nWordPress Toolkit\r\nDeveloper Pack\r\nSubscription Management\r\nAccount Management\r\nReseller Management', 'recurring', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2019-11-26 09:02:02', '2019-12-02 08:13:14'),
(36, '39', 'Backup Atom', '', 'recurring', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2020-01-13 03:54:39', '2020-01-13 03:56:55'),
(37, '41', 'Backup Proton', '', 'Monthly', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2020-01-13 03:55:17', '2020-03-28 05:22:14'),
(38, '42', 'Backup Big Bang', '', 'recurring', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2020-01-13 03:55:52', '2020-01-13 03:56:43'),
(41, '22,163,149,155,169,168,173,171,148', 'cPanel/WHM 1 Akun (SOLO) Dedicated Server', 'cPanel/WHM 1 Akun (SOLO) Dedicated Server', 'Monthly', 0, 1, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2020-04-18 05:34:24', '2020-05-06 04:43:16'),
(34, '11,125,130,129,128,127,132,131', '1 Akun cPanel', '1 Akun cPanel.', 'recurring', 0, 0, 0, 0, '', '', 0, 0, 'hostingaccount', '', 0, 0, '', '2019-12-12 07:49:56', '2019-12-12 07:55:00'),
(42, '52,4,53,5,6,54,49,1,50,2,51,3,103,114,100,65,35,66,36,67,37,68,38', 'Email Outgoing Filtering', 'Filtering Email Keluar mencegah IP dari daftar hitam (blacklist) sebagai hasil dari spam yang secara tidak sadar dikirim melalui akun yang disusupi, menawarkan perlindungan jaringan penuh dan reputasi IP.<br>\r\nIni memungkinkan email anda bisa masuk ke inbox tujuan.', 'recurring', 0, 1, 0, 0, '', 'on', 0, 0, 'other', 'kwspamexperts', 0, 112, '', '2020-05-12 08:52:53', '2020-05-15 02:46:09'),
(43, '22,52,4,53,5,6,54,49,1,50,2,51,3,103,114,100,65,35,66,36,67,37,68,38', 'Email Incoming Filtering', 'Incoming Filtering menawarkan perlindungan kotak masuk penuh terhadap spam, virus dan malware dengan bantuan algoritma canggih dan metode deteksi pola spam.', 'recurring', 0, 1, 0, 0, '', 'on', 0, 0, 'other', 'kwspamexperts', 0, 113, '', '2020-05-12 08:58:29', '2020-05-15 00:53:38'),
(44, '22,52,4,53,5,6,54,49,1,50,2,51,3,103,114,100,65,35,66,36,67,37,68,38', 'Bundle Incoming &amp; Outgoing Filtering', 'Spamexpert Incoming dan Outgoing filtering menyediakan proteksi pada inbox email anda terhadap spam, virus dan malware dan mencegang email anda masuk ke spam serta blacklist IP menawarkan perlindungan jaringan dan reputasi IP secara penuh.', 'recurring', 0, 1, 0, 0, '', 'on', 0, 0, 'hostingaccount', 'kwspamexperts', 0, 111, '', '2020-05-12 09:01:01', '2020-05-15 00:53:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city_id` int(10) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `url_image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `city_id`, `name`, `role`, `email`, `email_verified_at`, `url_image`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 48, 'owner', 'owner', 'owner@gmail.com', NULL, 'uploads/profile.jpg', '$2y$10$mPy.Z4doy.9i3e2J6mZxGeAiakatpmzsqhfikbCAsLJkTWdXFIsE.', NULL, '2020-01-02 06:53:10', '2020-01-02 06:53:10'),
(3, NULL, 'pengguna1', 'user', 'pengguna1@gmail.com', NULL, NULL, '$2y$10$Sxha4Hp1cwsdRO52LOJRcuV3R4HPDfbVSMAv2z1LvrKGXeRl4xvGu', NULL, '2020-01-07 13:14:05', '2020-01-07 13:14:05'),
(5, NULL, 'new Owner', 'owner', 'newowner@gmail.com', NULL, NULL, '$2y$10$hIyZTGxdCn8j.ljoK2sY/uAOxNZwmawKyB.r8lTKaa6S2WtBD5POe', NULL, '2020-01-07 14:03:24', '2020-01-07 14:03:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billings`
--
ALTER TABLE `billings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `owners`
--
ALTER TABLE `owners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proviences`
--
ALTER TABLE `proviences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbladdons`
--
ALTER TABLE `tbladdons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`(32));

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billings`
--
ALTER TABLE `billings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `owners`
--
ALTER TABLE `owners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `proviences`
--
ALTER TABLE `proviences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `tbladdons`
--
ALTER TABLE `tbladdons`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
