<?php

use Illuminate\Database\Seeder;


class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            'provience_id' => 5,
            'kabupaten' => 'Kepulauan Seribu',
            'city_name' => 'Pulau Pramuka',
        ]);
        DB::table('cities')->insert([
            'provience_id' => 5,
            'kabupaten' => 'Jakarta Barat',
            'city_name' => 'Kembangan',
        ]);
        DB::table('cities')->insert([
            'provience_id' => 5,
            'kabupaten' => 'Jakarta Pusat',
            'city_name' => 'Menteng',
        ]);
        DB::table('cities')->insert([
            'provience_id' => 5,
            'kabupaten' => 'Jakarta Selatan',
            'city_name' => 'Kebayoran Baru',
        ]);
        DB::table('cities')->insert([
            'provience_id' => 5,
            'kabupaten' => 'Jakarta Timur',
            'city_name' => 'Cakung',
        ]);
        DB::table('cities')->insert([
            'provience_id' => 5,
            'kabupaten' => 'Jakarta Utara',
            'city_name' => 'Koja',
        ]);
    }
}
