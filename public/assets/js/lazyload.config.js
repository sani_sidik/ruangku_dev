// lazyload config
var MODULE_CONFIG = {
    chat:           [
                      'http://komen.didev.id/libs/list.js/dist/list.js',
                      'http://komen.didev.id/libs/notie/dist/notie.min.js',
                      'http://komen.didev.id/assets/js/plugins/notie.js',
                      'http://komen.didev.id/assets/js/app/chat.js'
                    ],
    mail:           [
                      'http://komen.didev.id/libs/list.js/dist/list.js',
                      'http://komen.didev.id/libs/notie/dist/notie.min.js',
                      'http://komen.didev.id/assets/js/plugins/notie.js',
                      'http://komen.didev.id/assets/js/app/mail.js'
                    ],
    user:           [
                      'http://komen.didev.id/libs/list.js/dist/list.js',
                      'http://komen.didev.id/libs/notie/dist/notie.min.js',
                      'http://komen.didev.id/assets/js/plugins/notie.js',
                      'http://komen.didev.id/assets/js/app/user.js'
                    ],
    search:         [
                      'http://komen.didev.id/libs/list.js/dist/list.js',
                      'http://komen.didev.id/assets/js/app/search.js'
                    ],
    invoice:        [
                      'http://komen.didev.id/libs/list.js/dist/list.js',
                      'http://komen.didev.id/libs/notie/dist/notie.min.js',
                      'http://komen.didev.id/assets/js/app/invoice.js'
                    ],
    musicapp:       [
                      'http://komen.didev.id/libs/list.js/dist/list.js',
                      'http://komen.didev.id/assets/js/plugins/musicapp.js'
                    ],
    fullscreen:     [
                      'http://komen.didev.id/libs/jquery-fullscreen-plugin/jquery.fullscreen-min.js',
                      'http://komen.didev.id/assets/js/plugins/fullscreen.js'
                    ],
    jscroll:        [
                      'http://komen.didev.id/libs/jscroll/dist/jquery.jscroll.min.js'
                    ],
    countTo:        [
                      'http://komen.didev.id/libs/jquery-countto/jquery.countTo.js'
                    ],
    stick_in_parent:[
                      'http://komen.didev.id/libs/sticky-kit/dist/sticky-kit.min.js'
                    ],
    stellar:        [
                      'http://komen.didev.id/libs/jquery.stellar/jquery.stellar.min.js',
                      'http://komen.didev.id/assets/js/plugins/stellar.js'
                    ],
    masonry:        [
                      'http://komen.didev.id/libs/masonry-layout/dist/masonry.pkgd.min.js'
                    ],
    slick:          [
                      'http://komen.didev.id/libs/slick-carousel/slick/slick.css',
                      'http://komen.didev.id/libs/slick-carousel/slick/slick-theme.css',
                      'http://komen.didev.id/libs/slick-carousel/slick/slick.min.js'
                    ],
    sort:           [
                      'http://komen.didev.id/libs/html5sortable/dist/html.sortable.min.js',
                      'http://komen.didev.id/assets/js/plugins/sort.js'
                    ],
    apexcharts:     [
                      'http://komen.didev.id/libs/apexcharts/dist/apexcharts.min.js',
                      'http://komen.didev.id/assets/js/plugins/apexcharts.js'
                    ],
    chartjs:        [
                      'http://komen.didev.id/libs/moment/min/moment-with-locales.min.js',
                      'http://komen.didev.id/libs/chart.js/dist/Chart.min.js',
                      'http://komen.didev.id/libs/chart.js/dist/chart.ext.js',
                      'http://komen.didev.id/assets/js/plugins/chartjs.js'
                    ],
    chartist:       [
                      'http://komen.didev.id/libs/chartist/dist/chartist.min.css',
                      'http://komen.didev.id/libs/chartist/dist/chartist.min.js',
                      'http://komen.didev.id/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js',
                      'http://komen.didev.id/libs/chartist/dist/chartist.ext.js',
                      'http://komen.didev.id/assets/js/plugins/chartist.js'
                    ],
    dataTable:      [
                      'http://komen.didev.id/libs/datatables/media/js/jquery.dataTables.min.js',
                      'http://komen.didev.id/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js',
                      'http://komen.didev.id/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css',
                      'http://komen.didev.id/assets/js/plugins/datatable.js'
                    ],
    bootstrapTable: [
                      'http://komen.didev.id/libs/bootstrap-table/dist/bootstrap-table.min.js',
                      'http://komen.didev.id/libs/bootstrap-table/dist/extensions/export/bootstrap-table-export.min.js',
                      'http://komen.didev.id/libs/bootstrap-table/dist/extensions/mobile/bootstrap-table-mobile.min.js',
                      'http://komen.didev.id/assets/js/plugins/tableExport.min.js',
                      'http://komen.didev.id/assets/js/plugins/bootstrap-table.js'
                    ],
    bootstrapWizard:[
                      'http://komen.didev.id/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js'
                    ],
    dropzone:       [
                      'http://komen.didev.id/libs/dropzone/dist/min/dropzone.min.js',
                      'http://komen.didev.id/libs/dropzone/dist/min/dropzone.min.css'
                    ],
    typeahead:      [
                      'http://komen.didev.id/libs/typeahead.js/dist/typeahead.bundle.min.js',
                      'http://komen.didev.id/assets/js/plugins/typeahead.js'
                    ],
    datepicker:     [
                      "http://komen.didev.id/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
                      "http://komen.didev.id/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css",
                    ],
    daterangepicker:[
                      "http://komen.didev.id/libs/daterangepicker/daterangepicker.css",
                      'http://komen.didev.id/libs/moment/min/moment-with-locales.min.js',
                      "http://komen.didev.id/libs/daterangepicker/daterangepicker.js"
                    ],
    fullCalendar:   [
                      'http://komen.didev.id/libs/moment/min/moment-with-locales.min.js',
                      'http://komen.didev.id/libs/fullcalendar/dist/fullcalendar.min.js',
                      'http://komen.didev.id/libs/fullcalendar/dist/fullcalendar.min.css',
                      'http://komen.didev.id/libs/notie/dist/notie.min.js',
                      'http://komen.didev.id/assets/js/plugins/notie.js',
                      'http://komen.didev.id/assets/js/app/calendar.js'
                    ],
    parsley:        [
                      'http://komen.didev.id/libs/parsleyjs/dist/parsley.min.js'
                    ],
    select2:        [
                      'http://komen.didev.id/libs/select2/dist/css/select2.min.css',
                      'http://komen.didev.id/libs/select2/dist/js/select2.min.js',
                      'http://komen.didev.id/assets/js/plugins/select2.js'
                    ],
    summernote:     [
                      'http://komen.didev.id/libs/summernote/dist/summernote.css',
                      'http://komen.didev.id/libs/summernote/dist/summernote-bs4.css',
                      'http://komen.didev.id/libs/summernote/dist/summernote.min.js',
                      'http://komen.didev.id/libs/summernote/dist/summernote-bs4.min.js'
                    ],
    vectorMap:      [
                      'http://komen.didev.id/libs/jqvmap/dist/jqvmap.min.css',
                      'http://komen.didev.id/libs/jqvmap/dist/jquery.vmap.js',
                      'http://komen.didev.id/libs/jqvmap/dist/maps/jquery.vmap.world.js',
                      'http://komen.didev.id/libs/jqvmap/dist/maps/jquery.vmap.usa.js',
                      'http://komen.didev.id/libs/jqvmap/dist/maps/jquery.vmap.france.js',
                      'http://komen.didev.id/assets/js/plugins/jqvmap.js'
                    ],
    plyr:           [
                      'http://komen.didev.id/libs/plyrist/src/plyrist.css',
                      'http://komen.didev.id/libs/plyr/dist/plyr.polyfilled.min.js',
                      'http://komen.didev.id/libs/wavesurfer.js/dist/wavesurfer.min.js',
                      'http://komen.didev.id/libs/plyrist/src/plyrist.js',
                      'http://komen.didev.id/assets/js/plugins/plyr.js'
                    ]
  };

var MODULE_OPTION_CONFIG = {
  parsley: {
    errorClass: 'is-invalid',
    successClass: 'is-valid',
    errorsWrapper: '<ul class="list-unstyled text-sm mt-1 text-muted"></ul>'
  }
}
