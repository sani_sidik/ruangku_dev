//property function
function editProperty(id)
{
    $("body").addClass('modal-open');
    $("#editForm").addClass('show');
    $("#editForm").css({
        "display":"block",
        "background-color": "#4c464663",
        "z-index":"999999"
    });
    getItemProperty(id);
}

$("#close").click(function(){
    $("#editForm").hide();
    $("body").removeClass('modal-open');
});
function getItemProperty(id){
        $.ajax({
            type:"GET",
            data:"id="+id,
            dataType:"JSON",
            url:"http://komen.didev.id/getProperty",
            success:function(result){
                // console.log(result);
                if(result){
                    $.each(result,function(index, val){
                        $("#hisId").val(val.id);
                        $("#name").val(val.name);
                        $("#event-type").val(val.type);
                        if(val.type=='kontrakan'){
                            $("#kontrakan").attr('checked');
                        }else{$("#kos").attr('checked');}
                        $("#address").val(val.address);
                        $('#description').val(val.description);
                    });
                }
            },
            error:function(){
                alert("Failed to fetch Property, Please reload your page!");
            },

            complete:function(){

            }
        });
    }
    var token = $('meta[name="csrf-token"]').attr('content');
    function deleteProperty(num){
        var _token      = token;
        var id     = num;
                $.ajax({
                url: "http://komen.didev.id/owner/delete-property",
                type:'POST',
                data: {_token:_token, id:id},
                success: function () {
                    alert('Removed');
                    document.location.reload(true);
                    }
                });
}

//room function
function editRoom(id)
{
    $("#editForm").addClass('show');
    $("#editForm").css({
        "display":"block",
        "background-color": "#4c464663"
    });
    getItemRoom(id);
}

$("#close").click(function(){
    $("#editForm").hide();
});

function getItemRoom(id){
        $.ajax({
            type:"GET",
            data:"parent_id="+id,
            dataType:"JSON",
            url:"http://komen.didev.id/getRoom",
            success:function(result){
                // console.log(result);
                if(result){
                    $.each(result,function(index, val){
                        $("#hisId").val(val.id);
                        $("#name").val(val.name);
                        $("#price").val(val.price);
                    });
                }
            },
            error:function(){
                alert("Failed to fetch Room, Please reload your page!");
            },

            complete:function(){

            }
        });
    }

var token = $('meta[name="csrf-token"]').attr('content');;
function deleteRoom(num){
    var _token      = token;
    var room_id     = num;
            $.ajax({
            url: "http://komen.didev.id/owner/delete-room",
            type:'POST',
            data: {_token:_token, room_id:room_id},
            success: function () {
                alert('Removed');
                document.location.reload(true);
                }
            });
}