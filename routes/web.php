<?php

use Illuminate\Support\Facades\Crypt;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('home');
});
Route::get('owner-register', 'UserController@ownerRegistForm');
Route::post('owner-register', 'UserController@processRegister');

Route::group(['middleware' => 'auth'], function () {
    Route::get('user-dashboard', 'DashboardController@user');
    Route::get('owner-dashboard', 'DashboardController@owner');

    Route::prefix('owner')->group(function () {
        Route::get('account', 'UserController@myAccountOwner');
        Route::get('property', 'DashboardController@ownerProperty');
        Route::get('property/{id}', 'PropertyController@detailPropertty');
        Route::get('rental', 'DashboardController@ownerRental');
        Route::get('rental/{id}', 'DashboardController@ownerRentalid');
        Route::get('reports', 'DashboardController@ownerReports');
        Route::get('billing', 'DashboardController@ownerBilling');
        Route::post('add-property', 'PropertyController@addProperty');
        Route::post('edit-property', 'PropertyController@editProperty');
        Route::post('delete-property', 'PropertyController@deleteProperty');

        Route::post('add-room', 'RoomController@addRoom');
        Route::post('edit-room', 'RoomController@editRoom');
        Route::post('delete-room', 'RoomController@deleteRoom');
    });

    Route::get('getRoom', 'RoomController@getRoom');
    Route::get('getProperty', 'PropertyController@getProperty');
});
